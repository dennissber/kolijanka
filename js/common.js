$(function (){ 
	
  float_nav();
  seo_text_hidden();
  
  menu_nav();
  
  $( '#dl-menu' ).dlmenu();
  
  // set main variable
  var main_wrap  = $("#main_wrap");
  var top_bar    = $("#mobile_top_bar");
  var menu       = $("#mobile_nav");
  var class_open = "open_nav";

  /* menu_nav */
  function menu_nav(){

    $("body").on( "click", "#mobile_nav_btn", function(){
      if ( !menu.data('open') ){
        menu_open();
      } else {
        menu_close();
      }
    });

    $("body").on( "mousedown", ".main_wrap.open_nav", function(){
      if ( menu.data('open') ){
        menu_close();
      }
    });
    $("body").on( "touchmove", ".main_wrap.open_nav", function(){
      if ( menu.data('open') ){
        menu_close();
      }
    });
    $("body").on( "touchstart", ".main_wrap.open_nav", function(){
      if ( menu.data('open') ){
        menu_close();
      }
    });

    function menu_open(){
      main_wrap.addClass( class_open );
      top_bar.addClass( class_open );
      menu.addClass( class_open );
      menu.data('open', true);
    }
    function menu_close(){
      main_wrap.removeClass( class_open );
      top_bar.removeClass( class_open );
      menu.removeClass( class_open );
      menu.data('open', false);
    }

  };

  /*
  $(".catalogue__filter_size__wrap input").change( function() {
    this.form.submit();
  });
  $(".catalogue__filter_color__wrap input").change( function() {
    this.form.submit();
  });
  */




  /* search */
  $("body").on( "click", ".search_btn__js", function(e){
    var container = $(this).parent('.header__action__search_box');
    var input = container.find(".header__action__search_input");

    if(!container.hasClass('active')){
      container.addClass("active");
      e.preventDefault();
      input.focus();
    } 

  });


  /* search_mobile */
  function search_mobile(){
    var container = $(".mobile_top_bar__search_wrap");
      var input     = $(".header__action__search_input");
    container.open = false;
    $("body").on( "click", ".mobile_top_bar__search_btn", function(e){
      if ( !container.open ){
        $(container).fadeIn();
        input.focus();
        container.open = true;
      } else {
        $(container).fadeOut();
        container.open = false;
      }

    });

  }
  search_mobile();

  // if slick 
  if (typeof $(this).slick == 'function') {

    /* home_slider top */
    var home_slider = $('.home_slider_w').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      pauseOnHover: false,
      autoplaySpeed: 5000,
      fade: true,
      cssEase: 'linear',
    });

    $(".slider__prev").click(function() {
        home_slider.slick("slickPrev");
    }), $(".slider__next").click(function() {
        home_slider.slick("slickNext");
    });


    /* slider_tov */
    var slider_tov = $('.slider_tov').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      autoplaySpeed: 5000,
      responsive: [
        {
          breakpoint: 1400,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 1250,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });

    $(".slider_tov__arrows.prev").click(function() {
        slider_tov.slick("slickPrev");
    }), $(".slider_tov__arrows.next").click(function() {
        slider_tov.slick("slickNext");
    });
    
    
    /* single__gallery */
    var single__gallery__big = $('#single__gallery__big').slick({
      infinite: false,
      slidesToShow: 1,
      arrows: false,
      swipe: true,
      fade: true,
      cssEase: 'linear',
    });
    
    
    /* single__gallery */
    var single__gallery = $('#single__gallery').slick({
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      asNavFor: single__gallery__big,
      focusOnSelect: true,
      vertical: true,
      verticalSwiping: true,
      autoplaySpeed: 5000,
    });

    $("#single__gallery__prev").click(function() {
        single__gallery.slick("slickPrev");
    }), $("#single__gallery__next").click(function() {
        single__gallery.slick("slickNext");
    });
    
    
    /* single__gallery */
    var reviews_slider = $('#reviews_slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      vertical: true,
      verticalSwiping: true,
      autoplaySpeed: 5000,
      responsive: [
        {
          breakpoint: 700,
          settings: {
            slidesToShow: 1,
            vertical: false,
            verticalSwiping: false,
          }
        },
      ]
    });

    $("#reviews_slider__prev").click(function() {
        reviews_slider.slick("slickPrev");
    }), $("#reviews_slider__next").click(function() {
        reviews_slider.slick("slickNext");
    });
    

  }
  // END if slick

  /* simple_tabs */
  function simple_tab(){
    var tab_main = $("#simple_tabs");
    var head     = tab_main.find("#tab_head");
    var body     = tab_main.find("#tab_body");
    
    head.find("span").each(function(i){
      $(this).attr("select-id", i);
    });
    body.find(".tab_item").each(function(i){
      $(this).attr("tabs-id", i);
    });
    
    tab_main.on( "click", ".tab_head span", function(){
      var cur_select = $(this);
      var num        = cur_select.attr("select-id");
      /* head */
      head.find("span").removeClass("active");
      cur_select.addClass("active");
      /* body  */
      body.find('.tab_item').removeClass("active");
      body.find("[tabs-id='" + num + "']").addClass("active");
    });
    
  }
  simple_tab();



  /* float_nav */
  function float_nav(){

    var menu = $(".category_nav");
    var menu_t = menu.offset().top;

    var head_action = $(".header__action_w"); 

    // in load page
    var doc_r  = $(window).scrollTop();
    if ( doc_r > menu_t ){
      menu.addClass("fixed");
      head_action.addClass("fixed");
    }
    // in scroll
    $(window).scroll(function() {
      var doc_r  = $(window).scrollTop();
      if ( doc_r > menu_t ){
        head_action.addClass("fixed");
        menu.addClass("fixed");
      } else {
        head_action.removeClass("fixed");
        menu.removeClass("fixed");
      }
    });
  }


  /* seo_text_hidden */
  function seo_text_hidden(){
    var block = $(".seo_text__block");
    var block_h = block.height();

    if ( block_h > 200 ){
      block.addClass('hidden');
    }
  }

  $("body").on( "click", ".seo_text__btn", function(){
    var block = $(this).prev();
    var block_h = block.height();
    if ( !block.hasClass("open") ){
      block.addClass("open");
    } else {
      block.removeClass("open");
    }
  });

  
  /* modals */
  function modalOpen(modal){
    $('.'+modal).fadeIn();
    overlayOpen();
  }
  function modalClose(){
    $(".modalWindowWrap").fadeOut();
    overlayClose();
  }
  $("body").on( "click", "[data-modal]", function(){
    var modal = $(this).attr("data-modal");
    modalOpen(modal);
  });
  $("body").on( "click", function(event){
    var target = $(event.target);
    if ( target.hasClass('modalCell') || target.hasClass('modalWindowClose') ){
      modalClose();
    }
  });

  /* overlay */
  function overlayOpen(){
    $(".modalOverlay").fadeIn();
    $("html, body").addClass('no-scroll');
  }
  function overlayClose(){
    $(".modalOverlay").fadeOut();
    $("html, body").removeClass('no-scroll');
  }
  
  
  
  /* reCountCart */
   $(".count__item__input").keydown(function(event) {
     // Разрешаем нажатие клавиш backspace, del, tab и esc
     if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
        // Разрешаем выделение: Ctrl+A
       (event.keyCode == 65 && event.ctrlKey === true) || 
        // Разрешаем клавиши навигации: home, end, left, right
       (event.keyCode >= 35 && event.keyCode <= 39)) {
       return;
      } else {
        // Запрещаем всё, кроме клавиш цифр на основной клавиатуре, а также Num-клавиатуре
        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
          event.preventDefault(); 
      }   
     }
   });
  
  $(".count__item__btns").on( "click", "span", function(event){
    var btn = $(this);
    var input = $(this).parent().prev();
    if ( btn.hasClass('minus') && input.val() > 1 ){
      input.val(input.val()-1);
    }
    if ( btn.hasClass('plus') ){
      input.val(input.val()*1+1);
    }
  });
  /* END reCountCart */
  
  // chosen
  if ( $("#select_sity").length ){
    $("#select_sity").select2();
  }
  
  // type delivery
  $("body").on( "change", "#type_of_delivery", function(){
    var cur_val = $(this).val();
    var cur_block = $("#order_form__for__"+cur_val);
    $(".order_form__case").slideUp();
    cur_block.slideDown();
  });

  // order comment
  $("body").on( "click", "#form_row__add_comment", function(){
    $("#order_form__comment").slideToggle();
  });

  /* masked input */ 
  //var mask = "+7 (999) 999-99-99";
  //var placeholder = {'placeholder':'+7 (___) ___ __ __'};
  var mask = "+38(999) 999-99-99";
  var placeholder = {'placeholder':'+38(___) ___ __ __'};
  var user_phone = $('.user_phone_mask');

  user_phone.each(function(){
    $(this).mask(mask);
  });
  user_phone.attr(placeholder);
  
  
  
});